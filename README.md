```
#!shell

#!/usr/bin/env bash
# puppetdb-inventory.sh
# 
# returns a JSON formatted inventory from puppetdb
# 
# this script is not meant to be called directly, but only by ansible
# as part of a standalone or playbook run
# usage: ansible -i puppetdb-inventory.sh -m ping all
#        ansible-playbook -i puppetdb-inventory.sh kickPuppet.yaml


# figure out what network name docker-compose is using for puppetserver
PUPPETSERVER_NETWORK=$( docker ps -a |  awk '/puppet-server/{print $1}' | xargs docker inspect |  grep NetworkMode | cut -d: -f2 | sed 's/\"\(.*\)\",/\1/' )
docker run --net ${PUPPETSERVER_NETWORK} adamancini/ansible-puppetdb-dynamic-inventory
```